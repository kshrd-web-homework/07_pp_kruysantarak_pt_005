import React, { Component } from "react";

export default class Card extends Component {
  constructor() {
    super();
    this.state = {
      cardList: [
        {
          id: "1",
          src:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/480px-Unofficial_JavaScript_logo_2.svg.png",
          alt: "JavaScript Logo",
          title: "JavaScript ES6",
          text:
            "JavaScript is the world's most popular programming language. JavaScript is the programming language of the Web.",
        },
        {
          id: "2",
          src:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/HTML5_Badge.svg/600px-HTML5_Badge.svg.png",
          alt: "HTML Logo",
          title: "HTML5",
          text:
            "HTML stands for Hyper Text Markup Language. HTML is the standard markup language for creating Web pages.",
        },
        {
          id: "3",
          src: "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Devicon-css3-plain.svg/1200px-Devicon-css3-plain.svg.png",
          alt: "CSS Logo",
          title: "CSS3",
          text:
            "CSS stands for Cascading Style Sheets. CSS describes how HTML elements are to be displayed on screen in other media.",
        },
      ],
    };
  }

  remove_card = (deleteId) => {
    this.setState({
      cardList: this.state.cardList.filter((item) => item.id != deleteId),
    });
  };

  render() {
    let myCard = this.state.cardList.map((item) => {
      return (
        <div className="col-lg-4 col-md-8 col-sm-12">
          <div key={item.id}>
            <div className="card" style={{ width: "100%" }}>
              <img src={item.src} className="card-img-top" alt={item.alt} />
              <div className="card-body">
                <h5 className="card-title">{item.title}</h5>
                <p className="card-text">{item.text}</p>
                <a className="btn btn-danger" onClick={() => this.remove_card(item.id)}>
                  Delete Item
                </a>
              </div>
            </div>
          </div>
        </div>
      );
    });
    console.log(myCard);
    return <div className="row">{myCard}</div>;
  }
}