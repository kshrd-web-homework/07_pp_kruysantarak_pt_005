import React from "react";
import Card from "./components/Card";
import NavBar from './components/NavBar'
import "bootstrap/dist/css/bootstrap.css";

function App() {
  return (
    <div className="App">
      <div>
        <NavBar/>
      </div>
      <div className="container">
        <Card/>
      </div>
    </div>
  );
}

export default App;
